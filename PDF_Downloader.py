import requests

# PENSER A SUPPRIMER LE FICHIER DE LOG A CHAQUE UTILISATION

# Fonction de téléchargement de PDF
def download_pdf(url, file_path, code, constructeur):
    file_name = f"{constructeur}_{code}.pdf"
    # Requête HTTP en GET pour récupérer le PDF. {Verify = false} pour désactiver le besoin de certificat HTTP
    response = requests.get(url, stream=True, verify=False)
    # Si la requête HTTP renvoi quelque chose
    if response.status_code == 200:
        # Pour la progression du chargement
        total_size = int(response.headers.get('content-length', 0))
        downloaded_size = 0
        with open(f"{file_path}/{file_name}", 'wb') as file:
            for chunk in response.iter_content(4096): # 4096 : Taille des chunk en bit
                file.write(chunk)
                downloaded_size += len(chunk)
                progress = int((downloaded_size / total_size) * 100)
                print(f"Progression : {progress}%")
        with open(f"{file_path}/log.txt", 'a') as file: # a = append (écrire à la fin du fichier)
            file.write(f"[{code}] Téléchargement terminé. Fichier enregistré sous : {file_path}/{file_name}.\n")
            print(f"[{code}] Téléchargement terminé. Fichier enregistré sous : {file_path}/{file_name}.")
    else:
        with open(f"{file_path}/log.txt", 'a') as file:
            file.write(f"[{code}] Erreur d'écriture sur : {file_path}/{file_name}.\n")
        print("Erreur lors du téléchargement.")

# URL source [INPUT]
# HUAWEI == 'https://carrier.huawei.com/~/media/CNBG/Downloads/Product/Wireless-Network/Antenna/Passive-Antenna/%s/%s.pdf'
# AMPHENOL == 'https://amphenol-antennas.com/wp-content/uploads/datasheets/%s.pdf'
constructeur = input("Entrez le nom du constructeur (en majuscule) : ")

# FILE source [INPUT]
codes_file = input("Entrez le chemin d'accès du fichier où sont stockés les codes référents : ")

with open(codes_file, "r") as file:
    codes = file.read().splitlines()

# PATH destination [INPUT]
file_path = input("Entrez le chemin d'accès où télécharger les PDF : ")

base_url = input("Entrez l'URL de base pour télécharger les PDF : ")

if constructeur == 'AMPHENOL':
    # Parcourir les codes référents et télécharger tous les PDF
    for code in codes:
        if code[-1] == 'G' or code[-1] == 'B':
            if code[-2] == 'G' or code[-2] == 'N':
                url = base_url.replace('%s', code[:-2])
            else:
                url = base_url.replace('%s', code[:-1])
        else:
            url = base_url.replace('%s', code)
        
        download_pdf(url, file_path, code, constructeur)
else:
    for code in codes:
        url = base_url.replace('%s', code)
        download_pdf(url, file_path, code, constructeur)