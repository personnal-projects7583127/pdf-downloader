# PDF Downloader
This project is about a simple script allowing to automatically download all datasheets from a constructor's website based on reference code written on a text file.

## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Use](#use)
5. [Contact](#contact)

## General Info
**Release: 1.0 (2023-07-07).**
***
This README file was generated on [2023-07-07] by [PERRET Clément].

Last updated on: [2023-07-07].
***
This script is meant to be dynamic. Most of the information needed to run the script must be filled in by the user. This readme file will explain how to use the script. It is intended to be distributed to EIFFAGE industries. All rights reserved.

## Technologies
A list of technologies used within the project:
* [Python](https://www.python.org/): LTS version

## Installation
This script entirely work with Python using the lts version.

No installation required.

## Use
Download the executable in '/dist/PDF_Downloader/PDF_Downloader.exe' and launch it.

Then the script will ask for 4 information to run :
1. The name of the desire constructor in caps. Example : 'HUAWEI'
2. The path of the file where you wrote the reference code (1 per line). Make sure to add the file name after the path. Example : 'C:\Users\MyName\MyScript\MyCodes.txt'
3. The path of the folder where you want to download the PDF. Example : 'C:\Users\MyName\MyScript\MyPDF\'
4. The base URL of the constructor's datasheets. Make sure to use '%s' instead of the reference code to allow the script to replace these by the corresponding codes found in the file given previously. Example : 'https://carrier.huawei.com/~/media/CNBG/Downloads/Product/Wireless-Network/Antenna/Passive-Antenna/%s/%s.pdf'

## Contact
For any questions, feel free to contact me on :
* OUTLOOK: clement.perret@eiffage.com
* TEAMS: clement.perret@eiffage.com